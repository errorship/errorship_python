## To contribute:            

- You ideally should open an [issue](https://gitlab.com/errorship/errorship_python/-/issues) to discuss that which you would like to change.
- If the issue has been deliberated on and approved, you can now proceed.
- Create the necessary changes and remember to include tests and documentation(where necessary)
- Create a [merge request](https://gitlab.com/errorship/errorship_python/-/merge_requests)       
- Remember:  
  (a) There is still a possibility that the merge request may never actually be approved/accepted/merged as is(or at all).     
      This is dependent on the direction that the project maintainers want to take the project.      
