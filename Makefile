upload:
	@rm -rf build
	@rm -rf dist
	@sudo rm -rf errorship.egg-info
	@python setup.py sdist
	@python setup.py bdist_wheel
	@twine upload --config-file ~/.nonExistent_pypirc dist/* -r testpypi
	@pip install -U -i https://testpypi.python.org/pypi errorship

uploadprod:
	@rm -rf build
	@rm -rf dist
	@sudo rm -rf errorship.egg-info
	@python setup.py sdist
	@python setup.py bdist_wheel
	@twine upload --config-file ~/.nonExistent_pypirc dist/*
	@pip install -U errorship

test:
	@printf "\n\n coverage run tests:: \n"
	@coverage erase
	@coverage run --omit="*tests*,*examples/*,*.virtualenvs/*,*virtualenv/*,*.venv/*,*.venvpy2/*,*__init__*" -m unittest discover -v -s .
	@printf "\n\n coverage report:: \n"
	@coverage report --show-missing --fail-under=90
	@printf "\n\n coverage html:: \n"
	@coverage html --fail-under=90 --title=erroship

analysis:
	@printf "\n\t running black:\n"
	@black --line-length=100 --check . ||  { printf "\\n\\t black format your code."; exit 31; }
	@printf "\n\t running flake8:\n"
	@flake8 .
	@printf "\n\t running pylint:\n"
	@pylint --enable=E --disable=W,R,C errorship/ examples/ tests/
	@printf "\n\t running bandit:\n"
	@bandit -r --exclude .venv,.venvpy2 -ll errorship/ examples/ tests/
	@printf "\n\t running mypy:\n"
	@mypy --show-column-numbers --ignore-missing-imports errorship/ examples/ #--strict
	@printf "\n\t running pytype:\n"
	@pytype --verbosity 0 --python-version 3.7 --protocols --strict-import --keep-going errorship/
