# do not to pollute the global namespace.
# see: https://python-packaging.readthedocs.io/en/latest/testing.html

import socket
import logging
import unittest

import errorship

if errorship.py_version.PY2:
    import mock
else:
    from unittest import mock


class TestHandler(unittest.TestCase):
    """
    run tests as:
        python -m unittest discover -v -s . && \
        python2 -m unittest discover -v -s .
    run one testcase as:
        python -m unittest -v tests.test_handler.TestHandler.test_hello
    """

    def test_bad_hostname(self):
        with self.assertRaises(socket.gaierror) as raised_exception:
            errorship.Handler(
                datadog_agent_host="sadflkjsasf.i.nvali.d.",
                datadog_agent_port=8125,
                errorship_license_key="errorship_license_key",
            )
        self.assertIn("Name or service not known", str(raised_exception.exception))

    def test_bad_port(self):
        with self.assertRaises(TypeError) as raised_exception:
            errorship.Handler(
                datadog_agent_host="localhost",
                datadog_agent_port="8125",
                errorship_license_key="errorship_license_key",
            )
        self.assertIn(
            "`datadog_agent_port` should be of type:: `int`", str(raised_exception.exception)
        )

    def test_bad_errorship_license_key(self):
        with self.assertRaises(TypeError) as raised_exception:
            errorship.Handler(
                datadog_agent_host="localhost", datadog_agent_port=8125, errorship_license_key=8989
            )
        self.assertIn(
            "`errorship_license_key` should be of type:: `str`", str(raised_exception.exception)
        )

    def test_bad_tags(self):
        with self.assertRaises(TypeError) as raised_exception:
            errorship.Handler(
                datadog_agent_host="localhost",
                datadog_agent_port=8125,
                errorship_license_key="8989",
                tags=8125,
            )
        self.assertIn(
            "`tags` should be of type:: `dict` or `None`", str(raised_exception.exception)
        )

    def test_authorization_error(self):
        with mock.patch("errorship.http.Http.request") as mock_request:
            mock_request.return_value = errorship.http.Response(
                headers={}, body={"Authorized": False, "PricingPlan": ""}
            )
            with self.assertRaises(errorship.handler.AuthError) as raised_exception:
                errorship.Handler(
                    datadog_agent_host="localhost",
                    datadog_agent_port=8125,
                    errorship_license_key="errorship_license_key",
                )

            self.assertIn(
                errorship.Handler._auth_error, str(raised_exception.exception),
            )

    def test_authorization_success(self):
        with mock.patch("errorship.http.Http.request") as mock_request:
            mock_request.return_value = errorship.http.Response(
                headers={}, body={"Authorized": True, "PricingPlan": "Hobby"}
            )
            h = errorship.Handler(
                datadog_agent_host="localhost",
                datadog_agent_port=8125,
                errorship_license_key="errorship_license_key",
            )
            self.assertTrue(h._authorised)

    def test_authorization_success_on_exception(self):
        with mock.patch("errorship.http.Http.request") as mock_request:
            mock_request.side_effect = ValueError("Some exception")

            h = errorship.Handler(
                datadog_agent_host="localhost",
                datadog_agent_port=8125,
                errorship_license_key="errorship_license_key",
            )
            self.assertTrue(h._authorised)

    def test_tags_conversion(self):
        with mock.patch("errorship.http.Http.request") as mock_request:
            mock_request.return_value = errorship.http.Response(
                headers={}, body={"Authorized": True, "PricingPlan": "Hobby"}
            )
            h = errorship.Handler(
                datadog_agent_host="localhost",
                datadog_agent_port=8125,
                errorship_license_key="errorship_license_key",
                tags={"project": "accounting", "lifetime": 22, "env": "prod"},
            )
            self.assertEqual(h.tags, ["project:accounting", "lifetime:22", "env:prod"])

    def test_datadog_not_called_for_low_loglevel(self):
        with mock.patch("errorship.sock.Sock.send") as mock_send, mock.patch(
            "errorship.http.Http.request"
        ) as mock_request:
            mock_send.return_value = None
            mock_request.return_value = errorship.http.Response(
                headers={}, body={"Authorized": True, "PricingPlan": "Hobby"}
            )

            h = errorship.Handler(
                datadog_agent_host="localhost",
                datadog_agent_port=8125,
                errorship_license_key="errorship_license_key",
            )
            log_record = logging.LogRecord(
                name="someTestLogger",
                level=logging.INFO,
                pathname="/somePath/ala.py",
                lineno=45,
                msg="some-msg",
                args=(),
                exc_info=None,
                func=None,
            )
            h.emit(log_record)

            self.assertFalse(mock_send.called)

    def test_datadog_called_for_right_loglevel(self):
        with mock.patch("errorship.sock.Sock.send") as mock_send, mock.patch(
            "errorship.http.Http.request"
        ) as mock_request:
            mock_send.return_value = None
            mock_request.return_value = errorship.http.Response(
                headers={}, body={"Authorized": True, "PricingPlan": "Hobby"}
            )

            h = errorship.Handler(
                datadog_agent_host="localhost",
                datadog_agent_port=8125,
                errorship_license_key="errorship_license_key",
            )
            log_record = logging.LogRecord(
                name="someTestLogger",
                level=logging.ERROR,
                pathname="/somePath/ala.py",
                lineno=45,
                msg="some-msg",
                args=(),
                exc_info=None,
                func=None,
            )
            h.emit(log_record)

            self.assertTrue(mock_send.called)
            for i in ["hostName", "processName", "logMessage", "argv"]:
                self.assertIn(i, mock_send.call_args[0][0])

    def test_large_stack_traces(self):
        """
        tests that large stack traces are still handled/formatted correctly.
        """
        with mock.patch("errorship.http.Http.request") as mock_request:
            mock_request.return_value = errorship.http.Response(
                headers={}, body={"Authorized": True, "PricingPlan": "Hobby"}
            )
            handler = errorship.Handler(
                datadog_agent_host="localhost",
                datadog_agent_port=8125,
                errorship_license_key="errorship_license_key",
                tags={"project": "accounting", "lifetime": 22, "env": "prod"},
            )

            stack_trace = [
                "Traceback (most recent call last):\n",
                '  File "/home/someDirectory/errorship_org/errorship_python/.venv/lib/python3.7/site-packages/urllib3/connection.py", line 157, in _new_conn\n    (self._dns_host, self.port), self.timeout, **extra_kw\n',
                '  File "/home/someDirectory/errorship_org/errorship_python/.venv/lib/python3.7/site-packages/urllib3/util/connection.py", line 61, in create_connection\n    for res in socket.getaddrinfo(host, port, family, socket.SOCK_STREAM):\n',
                '  File "/usr/lib/python3.7/socket.py", line 748, in getaddrinfo\n    for res in _socket.getaddrinfo(host, port, family, type, proto, flags):\n',
                "socket.gaierror: [Errno -2] Name or service not known\n",
                "\nDuring handling of the above exception, another exception occurred:\n\n",
                "Traceback (most recent call last):\n",
                '  File "/home/someDirectory/errorship_org/errorship_python/.venv/lib/python3.7/site-packages/urllib3/connectionpool.py", line 672, in urlopen\n    chunked=chunked,\n',
                '  File "/home/someDirectory/errorship_org/errorship_python/.venv/lib/python3.7/site-packages/urllib3/connectionpool.py", line 376, in _make_request\n    self._validate_conn(conn)\n',
                '  File "/home/someDirectory/errorship_org/errorship_python/.venv/lib/python3.7/site-packages/urllib3/connectionpool.py", line 994, in _validate_conn\n    conn.connect()\n',
                '  File "/home/someDirectory/errorship_org/errorship_python/.venv/lib/python3.7/site-packages/urllib3/connection.py", line 334, in connect\n    conn = self._new_conn()\n',
                '  File "/home/someDirectory/errorship_org/errorship_python/.venv/lib/python3.7/site-packages/urllib3/connection.py", line 169, in _new_conn\n    self, "Failed to establish a new connection: %s" % e\n',
                "urllib3.exceptions.NewConnectionError: <urllib3.connection.VerifiedHTTPSConnection object at 0x7fd3e87681d0>: Failed to establish a new connection: [Errno -2] Name or service not known\n",
                "\nDuring handling of the above exception, another exception occurred:\n\n",
                "Traceback (most recent call last):\n",
                '  File "/home/someDirectory/errorship_org/errorship_python/.venv/lib/python3.7/site-packages/requests/adapters.py", line 449, in send\n    timeout=timeout\n',
                '  File "/home/someDirectory/errorship_org/errorship_python/.venv/lib/python3.7/site-packages/urllib3/connectionpool.py", line 720, in urlopen\n    method, url, error=e, _pool=self, _stacktrace=sys.exc_info()[2]\n',
                '  File "/home/someDirectory/errorship_org/errorship_python/.venv/lib/python3.7/site-packages/urllib3/util/retry.py", line 436, in increment\n    raise MaxRetryError(_pool, url, error or ResponseError(cause))\n',
                "urllib3.exceptions.MaxRetryError: HTTPSConnectionPool(host='g0ogle.com', port=443): Max retries exceeded with url: / (Caused by NewConnectionError('<urllib3.connection.VerifiedHTTPSConnection object at 0x7fd3e87681d0>: Failed to establish a new connection: [Errno -2] Name or service not known'))\n",
                "\nDuring handling of the above exception, another exception occurred:\n\n",
                "Traceback (most recent call last):\n",
                '  File "myApp.py", line 25, in call_google\n    response = requests.get(url, timeout=0.00001)\n',
                "  File \"/home/someDirectory/errorship_org/errorship_python/.venv/lib/python3.7/site-packages/requests/api.py\", line 75, in get\n    return request('get', url, params=params, **kwargs)\n",
                '  File "/home/someDirectory/errorship_org/errorship_python/.venv/lib/python3.7/site-packages/requests/api.py", line 60, in request\n    return session.request(method=method, url=url, **kwargs)\n',
                '  File "/home/someDirectory/errorship_org/errorship_python/.venv/lib/python3.7/site-packages/requests/sessions.py", line 533, in request\n    resp = self.send(prep, **send_kwargs)\n',
                '  File "/home/someDirectory/errorship_org/errorship_python/.venv/lib/python3.7/site-packages/requests/sessions.py", line 646, in send\n    r = adapter.send(request, **kwargs)\n',
                '  File "/home/someDirectory/errorship_org/errorship_python/.venv/lib/python3.7/site-packages/requests/adapters.py", line 516, in send\n    raise ConnectionError(e, request=request)\n',
                "requests.exceptions.ConnectionError: HTTPSConnectionPool(host='g0ogle.com', port=443): Max retries exceeded with url: / (Caused by NewConnectionError('<urllib3.connection.VerifiedHTTPSConnection object at 0x7fd3e87681d0>: Failed to establish a new connection: [Errno -2] Name or service not known'))\n",
            ]
            self.assertGreater(len("".join(stack_trace)), 4000)
            extra_info = "**argv:** ['myApp.py']\n**hostName:** home-prod.host.1\n**processName:** MainProcess\n**threadName:** MainThread\n**processId:** 16647\n**threadId:** 140548126639936\n**logMessage:** unable to call Google\n"

            text = handler._get_and_fit_text(stack_trace=stack_trace, extra_info=extra_info)

            # assert that the text is markdown formatted correctly at the beginning and at the end
            self.assertEqual(text[:14], handler.md_tmpl[:14])
            self.assertEqual(text[-3:], handler.md_tmpl[-3:])
            self.assertLess(len(text), 4000)

    def test_small_stack_traces(self):
        with mock.patch("errorship.http.Http.request") as mock_request:
            mock_request.return_value = errorship.http.Response(
                headers={}, body={"Authorized": True, "PricingPlan": "Hobby"}
            )
            handler = errorship.Handler(
                datadog_agent_host="localhost",
                datadog_agent_port=8125,
                errorship_license_key="errorship_license_key",
                tags={"project": "accounting", "lifetime": 22, "env": "prod"},
            )

        stack_trace = [
            "Traceback (most recent call last):\n",
            '  File "myApp.py", line 25, in call_google\n    response = requests.get(url, timeout=0.00001)\n',
            '  File "/home/someDirectory/errorship_org/errorship_python/.venv/lib/python3.7/site-packages/requests/adapters.py", line 516, in send\n    raise ConnectionError(e, request=request)\n',
        ]
        self.assertLess(len("".join(stack_trace)), 500)
        extra_info = "**argv:** ['myApp.py']\n**hostName:** home-prod.host.1\n**processName:** MainProcess\n**threadName:** MainThread\n**processId:** 16647\n**threadId:** 140548126639936\n**logMessage:** unable to call Google\n"

        text = handler._get_and_fit_text(stack_trace=stack_trace, extra_info=extra_info)

        # assert that the text is markdown formatted correctly at the beginning and at the end
        self.assertEqual(text[:14], handler.md_tmpl[:14])
        self.assertEqual(text[-3:], handler.md_tmpl[-3:])
        self.assertLess(len(text), 4000)


class TestHandlerIntegration(unittest.TestCase):
    """
    run tests as:
        python -m unittest discover -v -s . && \
        python2 -m unittest discover -v -s .
    run one testcase as:
        python -m unittest -v tests.test_handler.TestHandlerIntegration.test_hello
    """

    def _get_logger(self, name, level):
        logger = logging.getLogger(name)
        with mock.patch("errorship.http.Http.request") as mock_request:
            mock_request.return_value = errorship.http.Response(
                headers={}, body={"Authorized": True, "PricingPlan": "Hobby"}
            )
            handler = errorship.Handler(
                datadog_agent_host="localhost",
                datadog_agent_port=8125,
                errorship_license_key="errorship_license_key",
            )
        handler.setFormatter(logging.Formatter("%(message)s"))
        handler.setLevel(level)
        logger.addHandler(handler)
        logger.setLevel(level)
        return logger

    def test_datadog_not_called_for_low_logLevel(self):
        logger = self._get_logger(name="TestHandlerIntegration", level="DEBUG")
        with mock.patch("errorship.sock.Sock.send") as mock_send:
            mock_send.return_value = None
            logger.info("hello world")
            self.assertFalse(mock_send.called)

    def test_datadog_called_for_right_logLevel(self):
        logger = self._get_logger(name="TestHandlerIntegration", level="DEBUG")
        with mock.patch("errorship.sock.Sock.send") as mock_send:
            mock_send.return_value = None

            try:
                countries = {}
                countries["Canada"]
            except Exception as e:
                logger.exception("country_error. error={0}".format(e))

            self.assertTrue(mock_send.called)
            self.assertIn("KeyError", mock_send.call_args[0][0])
            self.assertIn("Canada", mock_send.call_args[0][0])
            for i in ["hostName", "processName", "argv"]:
                self.assertIn(i, mock_send.call_args[0][0])

    def test_logMessage_is_sent(self):
        logger = self._get_logger(name="TestHandlerIntegration", level="DEBUG")
        with mock.patch("errorship.sock.Sock.send") as mock_send:
            mock_send.return_value = None

            try:
                countries = {}
                countries["Canada"]
            except Exception as e:
                log_payload = {
                    "name": "country_error",
                    "error": str(e),
                    "error_kind": e.__class__.__name__,
                }
                logger.exception(log_payload)

            self.assertTrue(mock_send.called)
            self.assertIn("logMessage", mock_send.call_args[0][0])
            for i in [log_payload["name"], log_payload["error"], log_payload["error_kind"]]:
                self.assertIn(i, mock_send.call_args[0][0])
