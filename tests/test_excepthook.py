# do not to pollute the global namespace.
# see: https://python-packaging.readthedocs.io/en/latest/testing.html
import os
import sys
import logging
import unittest
import subprocess

import errorship

if errorship.py_version.PY2:
    import mock
else:
    from unittest import mock


class TestExceptHook(unittest.TestCase):
    """
    run tests as:
        python -m unittest discover -v -s . && \
        python2 -m unittest discover -v -s .
    run one testcase as:
        python -m unittest -v tests.test_excepthook.TestExceptHook.test_hello
    """

    def setUp(self):
        # re-set things to be what is in default python
        sys.excepthook = sys.__excepthook__

    def test_new_excepthook_is_added(self):
        original_except_hook = sys.excepthook
        self.assertEqual(original_except_hook.__name__, "excepthook")

        with mock.patch("errorship.http.Http.request") as mock_request:
            mock_request.return_value = errorship.http.Response(
                headers={}, body={"Authorized": True, "PricingPlan": "Hobby"}
            )
            h = errorship.Handler(
                datadog_agent_host="localhost",
                datadog_agent_port=8125,
                errorship_license_key="errorship_license_key",
            )
            self.assertTrue(h._authorised)

        new_except_hook = sys.excepthook
        self.assertEqual(new_except_hook.__name__, "errorship_sys_hook")

    def test_excepthook_reset(self):
        """
        test that if we have a third party that had added an excepthook after errorship
        and thus removed errorship's one;
        then errorship is able to re-add its own excepthook.
        The good thing is that errorship respects thirdparty excepthook's and will also yield control to them
        """
        with mock.patch("errorship.http.Http.request") as mock_request:
            mock_request.return_value = errorship.http.Response(
                headers={}, body={"Authorized": True, "PricingPlan": "Hobby"}
            )
            errorship.setup(
                datadog_agent_host="localhost",
                datadog_agent_port=8125,
                errorship_license_key="errorship_license_key",
            )
        new_except_hook = sys.excepthook
        self.assertEqual(new_except_hook.__name__, "errorship_sys_hook")

        def third_party_excepthook(exc_type, exc_value, exc_traceback):
            print("third_party_excepthook CALLED.")

        sys.excepthook = third_party_excepthook

        self.assertEqual(sys.excepthook.__name__, "third_party_excepthook")

        logging.error("Some Bad Error occured")
        self.assertEqual(sys.excepthook.__name__, "errorship_sys_hook")


class TestExceptHookIntegration(unittest.TestCase):
    """
    run tests as:
        python -m unittest discover -v -s . && \
        python2 -m unittest discover -v -s .
    run one testcase as:
        python -m unittest -v tests.test_excepthook.TestExceptHookIntegration.test_hello
    """

    def setUp(self):
        # re-set things to be what is in default python
        sys.excepthook = sys.__excepthook__

        self.third_party_hook_filename = "/{}/test_errorship_respects_third_party_excepthooks.py".format(
            "tmp"
        )
        self.third_party_hook_program = """
import sys
import errorship

def third_party_excepthook(exc_type, exc_value, exc_traceback):
    print("third_party_excepthook CALLED.")

# add a third party excepthook
sys.excepthook = third_party_excepthook

def mock_errorship_Http_request():
    return errorship.http.Response(
                headers={}, body={"Authorized": True, "PricingPlan": "Hobby"}
            )

# mock success call to errorship.com
errorship.http.Http.request = mock_errorship_Http_request

# now create an errorship handler which will also add an errorship_excepthook
h = errorship.Handler(
    datadog_agent_host="localhost",
    datadog_agent_port=8125,
    errorship_license_key="errorship_license_key",
    tags={"env": "production", "project": "accounting"},
)

# run code that has an uncaught exception.
# this should cause the errorship excepthook to be called.
# this should also cause the third party excepthook to be also called(if errorship behaved correctly)
x = {}
x["nonExistentKey"]
"""

        self.default_hook_filename = "/{}/test_errorship_respects_default_excepthooks.py".format(
            "tmp"
        )
        self.default_hook_program = """
import sys
import errorship

def mock_errorship_Http_request():
    return errorship.http.Response(
                headers={}, body={"Authorized": True, "PricingPlan": "Hobby"}
            )

# mock success call to errorship.com
errorship.http.Http.request = mock_errorship_Http_request

# now create an errorship handler which will also add an errorship_excepthook
h = errorship.Handler(
    datadog_agent_host="localhost",
    datadog_agent_port=8125,
    errorship_license_key="errorship_license_key",
    tags={"env": "production", "project": "accounting"},
)

# run code that has an uncaught exception.
# this should cause the errorship excepthook to be called.
# this should also cause the default PYTHON excepthook to be also called.
x = {}
x["nonExistentKey"]
"""

        self.send_to_datadog_filename = "/{}/test_datadog_is_called.py".format("tmp")
        self.send_to_datadog_program = """
import sys
import errorship

def mock_errorship_Http_request():
    return errorship.http.Response(
                headers={}, body={"Authorized": True, "PricingPlan": "Hobby"}
            )

# mock success call to errorship.com
errorship.http.Http.request = mock_errorship_Http_request

# now create an errorship handler which will also add an errorship_excepthook
h = errorship.Handler(
    datadog_agent_host="localhost",
    datadog_agent_port=8125,
    errorship_license_key="errorship_license_key",
    tags={"env": "production", "project": "accounting"},
)

# mock send to datadog
def mock_errorship_sock_Sock_send(self, data):
    print("mock_errorship_sock_Sock_send CALLED with {}".format(data))

errorship.sock.Sock.send = mock_errorship_sock_Sock_send

# run code that has an uncaught exception.
# this should cause the errorship excepthook to be called.
# this should also cause the default PYTHON excepthook to be also called.
x = {}
x["nonExistentKey"]
"""

    def tearDown(self):
        if os.path.exists(self.third_party_hook_filename):
            os.remove(self.third_party_hook_filename)

        if os.path.exists(self.default_hook_filename):
            os.remove(self.default_hook_filename)

        if os.path.exists(self.send_to_datadog_filename):
            os.remove(self.send_to_datadog_filename)

    # TODO: fix python2 equivalent
    @unittest.skipIf(errorship.py_version.PY2, "python2 does not have `subprocess.run`")
    def test_errorship_respects_third_party_excepthooks(self):
        with open(self.third_party_hook_filename, "w+") as fp:
            fp.write(self.third_party_hook_program)

        res = subprocess.run(
            ["python", self.third_party_hook_filename],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            timeout=5,
        )

        self.assertEqual(b"", res.stderr)
        self.assertIn("third_party_excepthook CALLED.".encode(), res.stdout)

    # TODO: fix python2 equivalent
    @unittest.skipIf(errorship.py_version.PY2, "python2 does not have `subprocess.run`")
    def test_errorship_respects_default_excepthooks(self):
        with open(self.default_hook_filename, "w+") as fp:
            fp.write(self.default_hook_program)

        with mock.patch("errorship.http.Http.request") as mock_request:
            mock_request.return_value = errorship.http.Response(
                headers={}, body={"Authorized": True, "PricingPlan": "Hobby"}
            )
            res = subprocess.run(
                ["python", self.default_hook_filename],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                timeout=5,
            )

            self.assertEqual(b"", res.stdout)
            self.assertIn("KeyError".encode(), res.stderr)
            self.assertIn("nonExistentKey".encode(), res.stderr)

    # TODO: fix python2 equivalent
    @unittest.skipIf(errorship.py_version.PY2, "python2 does not have `subprocess.run`")
    def test_datadog_is_called(self):
        with open(self.send_to_datadog_filename, "w+") as fp:
            fp.write(self.send_to_datadog_program)

        with mock.patch("errorship.http.Http.request") as mock_request:
            mock_request.return_value = errorship.http.Response(
                headers={}, body={"Authorized": True, "PricingPlan": "Hobby"}
            )
            res = subprocess.run(
                ["python", self.send_to_datadog_filename],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                timeout=5,
            )

            self.assertIn("mock_errorship_sock_Sock_send CALLED with".encode(), res.stdout)
            self.assertIn("KeyError".encode(), res.stdout)
            for i in ["hostName", "processName", "argv"]:
                self.assertIn(i.encode(), res.stdout)

            # default excepthook is still invoked
            self.assertIn("KeyError".encode(), res.stderr)
            self.assertIn("nonExistentKey".encode(), res.stderr)
