# do not to pollute the global namespace.
# see: https://python-packaging.readthedocs.io/en/latest/testing.html

import unittest

import errorship


if errorship.py_version.PY2:
    import mock
else:
    from unittest import mock


class TestHttp(unittest.TestCase):
    """
    run tests as:
        python -m unittest discover -v -s .
    run one testcase as:
        python -m unittest -v tests.test_http.TestHttp.test_hello
    """

    def test_success_request(self):
        url = "https://errorship.com/api/?errorshipLicensekey={errorship_license_key}".format(
            errorship_license_key="example_license_key"
        )
        http = errorship.http.Http(url=url, timeout=1)
        res = http.request()

        if errorship.py_version.PY2:
            self.assertEqual(res.headers.get("content-type"), "application/json")
        else:
            self.assertEqual(res.headers.get("Content-Type"), "application/json")
        self.assertFalse(res.Authorized)
        self.assertEqual(res.PricingPlan, "")

    def test_timeout(self):
        def make_req():
            http = errorship.http.Http(url="https://example.com/", timeout=0.0000001)
            http.request()

        with self.assertRaises(Exception) as raised_exception:
            make_req()
        self.assertIn("urlopen error", str(raised_exception.exception))

    def test_success_auth(self):
        with mock.patch("errorship.http.Http.request") as mock_request:
            mock_request.return_value = errorship.http.Response(
                headers={}, body={"Authorized": True, "PricingPlan": "Hobby"}
            )
            http = errorship.http.Http(url="http://example.com", timeout=1)
            authorized, _ = http.req_n_auth()

            self.assertTrue(authorized)

    def test_failed_auth(self):
        with mock.patch("errorship.http.Http.request") as mock_request:
            mock_request.return_value = errorship.http.Response(
                headers={}, body={"Authorized": False, "PricingPlan": ""}
            )
            http = errorship.http.Http(url="http://example.com", timeout=1)
            authorized, _ = http.req_n_auth()

            self.assertFalse(authorized)

    def test_server_responds_with_NOT_authorized(self):
        with mock.patch("errorship.http.Http.request") as mock_request:
            mock_request.return_value = errorship.http.Response(
                headers={}, body={"Authorized": False, "PricingPlan": ""}
            )

            http = errorship.http.Http(url="http://example.com", timeout=1)
            authorized, _ = http.req_n_auth()

            self.assertFalse(authorized)

    def test_server_responds_with_authorized(self):
        with mock.patch("errorship.http.Http.request") as mock_request:
            mock_request.return_value = errorship.http.Response(
                headers={}, body={"Authorized": True, "PricingPlan": "Hobby"}
            )

            http = errorship.http.Http(url="http://example.com", timeout=1)
            authorized = http.req_n_auth()

            self.assertTrue(authorized)

    def test_server_responds_with_exception(self):
        with mock.patch("errorship.http.Http.request") as mock_request:
            mock_request.side_effect = ValueError("Some exception")

            http = errorship.http.Http(url="http://example.com", timeout=1)
            authorized = http.req_n_auth()

            self.assertTrue(authorized)

    def test_custom_url_schemes_banned(self):
        # https://bandit.readthedocs.io/en/latest/blacklists/blacklist_calls.html#b310-urllib-urlopen
        with self.assertRaises(ValueError) as raised_exception:
            errorship.http.Http(url="file:///somePath/someFile.txt")
        self.assertIn(
            "`url` should be a http:// or https:// scheme", str(raised_exception.exception)
        )
