# do not to pollute the global namespace.
# see: https://python-packaging.readthedocs.io/en/latest/testing.html

import socket
import unittest
import threading

import errorship

if errorship.py_version.PY2:
    import mock
else:
    from unittest import mock


class TestSock(unittest.TestCase):
    """
    run tests as:
        python -m unittest discover -v -s .
    run one testcase as:
        python -m unittest -v tests.test_sock.TestSock.test_hello
    """

    def setUp(self):
        # google's dns service runs on udp
        self.host = "8.8.8.8"
        self.port = 53
        self.socket_client = errorship.sock.Sock(host=self.host, port=self.port, DOING_TESTS=True)

    def tearDown(self):
        self.socket_client.close()

    def test_bad_instantiation(self):
        def insta():
            errorship.sock.Sock(host="host", port="port")

        self.assertRaises(TypeError, insta)

    def test_client_success(self):
        msg = "hello world"
        self.socket_client.send(msg)

    def test_race_condition(self):
        """
        to reproduce the race condition run this test multiple times:

            #!/bin/bash
            for i in {1..8}
            do
                echo "running race condition test"
                python -m unittest -v tests.test_sock.TestSock.test_race_condition
            done
        TODO: add the above script to CI
        """
        last_thread = None
        num_threads = 21
        thread_name = "test_race_condition_Thread-{0}"
        msg = "hello-{0}"

        for i in range(0, num_threads + 1):
            th = threading.Thread(
                name=thread_name.format(i),
                target=self.socket_client.send,
                kwargs={"data": msg.format(i)},
            )
            th.start()
            last_thread = th

        self.assertEqual(last_thread.name, thread_name.format(num_threads))

    def test_timeout_terminates_socket(self):
        socket_client = errorship.sock.Sock(host=self.host, port=self.port, DOING_TESTS=False)
        with mock.patch("socket.socket.send") as mock_socket_send:
            mock_socket_send.side_effect = socket.timeout("Some timeout")

            msg = "hello world"
            socket_client.send(msg)
            self.assertIsNone(socket_client.socket)

    def test_protocol_error_terminates_socket(self):
        socket_client = errorship.sock.Sock(host=self.host, port=self.port, DOING_TESTS=False)
        with mock.patch("socket.socket.send") as mock_socket_send:
            mock_socket_send.side_effect = socket.error("Some socket error")

            msg = "hello world"
            socket_client.send(msg)
            self.assertIsNone(socket_client.socket)

    def test_unknown_error_terminates_socket(self):
        socket_client = errorship.sock.Sock(host=self.host, port=self.port, DOING_TESTS=False)

        class SomeRandomError(Exception):
            pass

        with mock.patch("socket.socket.send") as mock_socket_send:
            mock_socket_send.side_effect = SomeRandomError("Some random error")

            msg = "hello world"
            socket_client.send(msg)
            self.assertIsNone(socket_client.socket)
