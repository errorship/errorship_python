The errorship license (the "License")    
Copyright (c) 2020 errorship.    

With regard to the errorship Software:    

This software and associated documentation files (the "Software") may    
only be used in production, if you (and any entity that you represent)    
have a valid errorship subscription available at https://errorship.com.    
Subject to the foregoing sentence, you are free to modify this Software    
and publish patches to the Software.   
You agree that all such modifications and/or patches may only be used,    
copied, modified, displayed, distributed, or otherwise exploited with    
a valid errorship subscription.    
Notwithstanding the foregoing, you may copy and modify the Software    
for development and testing purposes, without requiring a subscription.       

The full text of this License shall be included in all copies or    
substantial portions of the Software.    

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR    
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER    
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,    
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE    
SOFTWARE.    

For all third party components incorporated into the errorship Software, those    
components are licensed under the original license provided by the owner of the    
applicable component.    
