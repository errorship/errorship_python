import os
import django
import errorship
import unittest

from django.test.client import Client
from django.core.urlresolvers import reverse

if errorship.py_version.PY2:
    import mock
else:
    from unittest import mock  # type: ignore


class TestDjangoApp(unittest.TestCase):
    """
    run tests as:
        python -m unittest discover -v -s .
    run one testcase as:
        python -m unittest -v examples.django.test_app.TestDjangoApp.test_hello
    """

    def setUp(self):
        with mock.patch("errorship.http.Http.request") as mock_request:
            mock_request.return_value = errorship.http.Response(
                headers={}, body={"Authorized": True, "PricingPlan": "Hobby"}
            )

            os.environ.setdefault("DJANGO_SETTINGS_MODULE", "examples.django.settings")
            django.setup()

    def test_errorship_integration(self):

        with mock.patch("errorship.sock.Sock.send") as mock_send, mock.patch(
            "errorship.http.Http.request"
        ) as mock_request:
            mock_request.return_value = errorship.http.Response(
                headers={}, body={"Authorized": True, "PricingPlan": "Hobby"}
            )

            client = Client()
            name = "Jimmy"
            response = client.get(reverse("send_email_view", kwargs={"name": name}))
            self.assertEqual(response.status_code, 200)

            self.assertTrue(mock_send.called)
            self.assertIn("KeyError", mock_send.call_args[0][0])
            self.assertIn(name, mock_send.call_args[0][0])
            for i in ["hostName", "processName", "argv"]:
                self.assertIn(i, mock_send.call_args[0][0])
