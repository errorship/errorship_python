from django.conf.urls import url

from examples.django.views import send_email_view

urlpatterns = (
    # url(r"^$", send_email_view, name="send_email_view"),
    url(r"^(?P<name>[a-zA-Z0-9_.-]+)/?$", send_email_view, name="send_email_view"),
)
