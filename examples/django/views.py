import logging
from django.http import HttpResponse


def send_email_view(request, name):
    logger = logging.getLogger("myApp")
    logger.info("start_send_email")

    try:
        all_messages = {}
        _ = all_messages[name]  # keyError
    except Exception as e:
        logger.exception("send_email_error. error={0}".format(e))

    return HttpResponse("<h1>Hello from django</h1>")
