# setup settings
DEBUG = True
SECRET_KEY = "Replace-With-a-Random-Secure-And-Private-String"
ALLOWED_HOSTS = "*"
ROOT_URLCONF = "examples.django.urls"

LOGGING = {
    "version": 1,
    "handlers": {
        "console": {"level": "INFO", "class": "logging.StreamHandler"},
        # Integrate Django logging with errorship
        "errorship": {
            "level": "ERROR",
            "class": "errorship.Handler",
            "datadog_agent_host": "localhost",
            "datadog_agent_port": 8125,
            "errorship_license_key": "MyErrorshipLicensekey",
            "tags": {"env": "production", "project": "accounting"},
        },
    },
    "loggers": {
        "django": {"handlers": ["console", "errorship"], "level": "INFO"},
        "myApp": {"handlers": ["console", "errorship"], "level": "INFO"},
    },
}
