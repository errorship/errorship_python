import os
import sys
import time
import logging
import errorship


class MyApp:
    """
    This is an example that shows how to use errorship in regular python.

    Usage:
        app = MyApp(name="Jimmy")
        app.send_email()
    """

    def __init__(self, name):
        self.name = name
        self.logger = self.get_logger()

    def send_email(self):
        self.logger.info("begin sending email to: {0}".format(self.name))
        try:
            all_messages = {}
            _ = all_messages[self.name]  # keyError
        except Exception as e:
            self.logger.exception("send_email_error. error={0}".format(e))

    @staticmethod
    def get_logger():
        logger = logging.getLogger("MyLoggerName")
        level = "INFO"
        handler = errorship.Handler(
            datadog_agent_host=os.environ.get("DD_HOST", "localhost"),
            datadog_agent_port=8125,
            errorship_license_key="MyErrorshipLicensekey",
            tags={"env": "production", "project": "accounting"},
        )
        handler.setFormatter(logging.Formatter("%(message)s"))
        handler.setLevel(level)
        logger.addHandler(handler)
        logger.setLevel(level)
        return logger


if __name__ == "__main__":
    app = MyApp(name="Jimmy")
    counter = 0
    while True:
        print("counter: ", counter)
        app.send_email()
        time.sleep(4)
        counter = counter + 1
        if counter > 11:
            sys.exit(0)
