import os
import logging
import errorship

handler = errorship.Handler(
    datadog_agent_host="localhost",
    datadog_agent_port=8125,
    errorship_license_key=os.environ["errorship_license_key"],
    # tags will be added to the exceptions sent to datadog
    tags={"env": "production", "deployment_version": "v12.56", "myTag": "myTagValue"},
)
logging.getLogger("").addHandler(handler)


def send_request(url):
    import requests

    try:
        response = requests.get(url, timeout=0.00001)
        print("response is: ", response)
    except Exception:
        logging.error("unable to call {url}".format(url=url))


while True:
    import time

    send_request(url="https://google.com")
    time.sleep(7)
# end
