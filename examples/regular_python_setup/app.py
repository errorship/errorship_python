import os
import sys
import time
import logging
import errorship


class MyApp:
    """
    This is an example that shows how to use errorship in regular python.

    Usage:
        app = MyApp(name="Jimmy")
        app.send_email()
    """

    def __init__(self, name):
        errorship.setup(
            datadog_agent_host=os.environ.get("DD_HOST", "localhost"),
            datadog_agent_port=8125,
            errorship_license_key="MyErrorshipLicensekey",
            tags={"env": "production", "project": "accounting"},
        )
        self.name = name

    def send_email(self):
        name = self.name
        logger = logging.getLogger("MyLoggerName")
        logger.info("begin sending email to: {0}".format(name))

        try:
            all_messages = {}
            _ = all_messages[name]  # keyError
        except Exception as e:
            logger.exception("send_email_error. error={0}".format(e))


if __name__ == "__main__":
    app = MyApp(name="Cool")
    counter = 0
    while True:
        print("counter: ", counter)
        app.send_email()
        time.sleep(4)
        counter = counter + 1
        if counter > 11:
            sys.exit(0)
