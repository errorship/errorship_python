import errorship
import unittest

from .app import MyApp


if errorship.py_version.PY2:
    import mock
else:
    from unittest import mock  # type: ignore


class TestRegularPythonSetupApp(unittest.TestCase):
    """
    run tests as:
        python -m unittest discover -v -s .
    run one testcase as:
        python -m unittest -v examples.regular_python_setup.test_app.TestRegularPythonSetupApp.test_hello
    """

    def test_errorship_integration(self):
        with mock.patch("errorship.sock.Sock.send") as mock_send, mock.patch(
            "errorship.http.Http.request"
        ) as mock_request:
            mock_request.return_value = errorship.http.Response(
                headers={}, body={"Authorized": True, "PricingPlan": "Hobby"}
            )

            name = "Jeff"
            app = MyApp(name=name)
            app.send_email()

            self.assertTrue(mock_send.called)
            self.assertIn("KeyError", mock_send.call_args[0][0])
            self.assertIn(name, mock_send.call_args[0][0])
            for i in ["hostName", "processName", "argv"]:
                self.assertIn(i, mock_send.call_args[0][0])
