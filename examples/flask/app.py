import errorship
from flask import Flask


app = Flask(__name__)
# run as:
# python app.py


@app.route("/<name>")
def send_email_view(name):
    app.logger.info("start_send_email")  # pylint: disable=E1101
    try:
        all_messages = {}
        _ = all_messages[name]  # keyError
    except Exception as e:
        app.logger.exception("send_email_error. error={0}".format(e))  # pylint: disable=E1101
    return "<h1>Hello from Flask</h1>"


if __name__ == "__main__":
    handler = errorship.Handler(
        datadog_agent_host="localhost",
        datadog_agent_port=8125,
        errorship_license_key="MyErrorshipLicensekey",
        tags={"env": "production", "project": "accounting"},
    )
    app.logger.addHandler(handler)  # pylint: disable=E1101
    app.run()
