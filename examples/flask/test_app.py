import errorship
import unittest


from .app import app


if errorship.py_version.PY2:
    import mock
else:
    from unittest import mock  # type: ignore


class TestFlaskApp(unittest.TestCase):
    """
    run tests as:
        python -m unittest discover -v -s .
    run one testcase as:
        python -m unittest -v examples.flask.test_app.TestFlaskApp.test_hello
    """

    def setUp(self):
        with mock.patch("errorship.http.Http.request") as mock_request:
            mock_request.return_value = errorship.http.Response(
                headers={}, body={"Authorized": True, "PricingPlan": "Hobby"}
            )

            handler = errorship.Handler(
                datadog_agent_host="localhost",
                datadog_agent_port=8125,
                errorship_license_key="MyErrorshipLicensekey",
                tags={"env": "production", "project": "accounting"},
            )
            app.logger.addHandler(handler)  # pylint: disable=E1101

            app.config["TESTING"] = True
            app.config["DEBUG"] = True
            self.client = app.test_client()

    def test_errorship_integration(self):
        with mock.patch("errorship.sock.Sock.send") as mock_send, mock.patch(
            "errorship.http.Http.request"
        ) as mock_request:
            mock_request.return_value = errorship.http.Response(
                headers={}, body={"Authorized": True, "PricingPlan": "Hobby"}
            )

            name = "Jimmy"
            response = self.client.get("/{name}".format(name=name))
            self.assertEqual(response.status_code, 200)

            self.assertTrue(mock_send.called)
            self.assertIn("KeyError", mock_send.call_args[0][0])
            self.assertIn(name, mock_send.call_args[0][0])
            for i in ["hostName", "processName", "argv"]:
                self.assertIn(i, mock_send.call_args[0][0])
