import errorship
from logging.config import dictConfig


def errorship_setup_alternative_one():
    # See flask documentation:
    # http://flask.palletsprojects.com/en/1.1.x/logging/#basic-configuration
    dictConfig(
        {
            "version": 1,
            "formatters": {
                "default": {"format": "[%(asctime)s] %(levelname)s in %(module)s: %(message)s"}
            },
            "handlers": {
                "wsgi": {
                    "class": "logging.StreamHandler",
                    "stream": "ext://flask.logging.wsgi_errors_stream",
                    "formatter": "default",
                },
                # Integrate Flask logging with errorship
                "errorship": {
                    "level": "ERROR",
                    "class": "errorship.Handler",
                    "datadog_agent_host": "localhost",
                    "datadog_agent_port": 8125,
                    "errorship_license_key": "MyErrorshipLicensekey",
                    "tags": {"env": "production", "project": "accounting"},
                },
            },
            "root": {"level": "INFO", "handlers": ["wsgi", "errorship"]},
        }
    )


def errorship_setup_alternative_two():
    errorship.setup(
        datadog_agent_host="localhost",
        datadog_agent_port=8125,
        errorship_license_key="MyErrorshipLicensekey",
        tags={"env": "production", "project": "accounting"},
    )


def errorship_setup_alternative_three(app):
    handler = errorship.Handler(
        datadog_agent_host="localhost",
        datadog_agent_port=8125,
        errorship_license_key="MyErrorshipLicensekey",
        tags={"env": "production", "project": "accounting"},
    )
    app.logger.addHandler(handler)
