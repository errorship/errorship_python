FROM python:3.7

RUN mkdir -p /workdir
COPY . /workdir
WORKDIR /workdir
RUN pip install -e .
